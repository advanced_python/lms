# Generated by Django 3.2.11 on 2022-02-18 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core_lms', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='logger',
            name='id',
        ),
        migrations.AlterField(
            model_name='logger',
            name='user',
            field=models.CharField(max_length=20, primary_key=True, serialize=False),
        ),
    ]
