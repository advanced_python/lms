import sys
import time

from core_lms.models import Logger


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response


    def __call__(self, request):
        start = time.time()
        response = self.get_response(request)
        response.headers['X-Response-Time'] = str(round(time.time() - start, 3))
        return response

class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response


    def __call__(self, request):
        request_start = time.time()
        response = self.get_response(request)

        l = Logger()

        # if request.user.is_authenticated:
        #     l.user = request.user
        #     print(request.user)

        l.path = request.path
        l.create_date = request_start
        l.execution_time = str(round(time.time() - request_start, 3))
        l.query_params = request.environ['QUERY_STRING']
        l.save()

        return response

