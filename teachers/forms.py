import django_filters
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from teachers.models import Teacher


class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = {
            'first_name': ['iexact'],
            'last_name': ['iexact'],
            'email': ['exact'],
        }


class TeacherBaseForm(ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        has_phone_number_qs = Teacher.objects.filter(phone_number=phone_number)

        if self.instance:
            has_phone_number_qs = has_phone_number_qs.exclude(id=self.instance.id)

        if has_phone_number_qs.exists():
            raise ValidationError("Phone number is not unique")

        return phone_number

    def clean_email(self):
        email = self.cleaned_data['email']

        banned_domains = [
            "gmail",
            "rambler",
        ]

        domains = []

        for service in banned_domains:
            domains.append("@{0}".format(service))

        ads_tuple = tuple(domains)

        if email[:email.rfind('.')].endswith(ads_tuple):
            raise ValidationError("{0} is a banned domain!".format(email))
        return email


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta:
        model = Teacher
        exclude = ['birth_date']
