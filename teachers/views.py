from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView, DeleteView, CreateView, ListView

from teachers.forms import TeacherCreateForm, TeacherUpdateForm, TeacherFilter
from teachers.models import Teacher


class TeacherListView(ListView):
    model = Teacher
    template_name = 'teachers/list_teachers.html'
    paginate_by = 10

    def get_filter(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return TeacherFilter(data=self.request.GET, queryset=queryset)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.select_related('group__headman').order_by('-id')
        filter_ = self.get_filter(queryset)
        return filter_.qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        return context


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    success_url = reverse_lazy('teachers:list_teachers')
    form_class = TeacherCreateForm
    template_name = 'teachers/create_teacher.html'


class TeacherEditView(LoginRequiredMixin, UpdateView):
    model = Teacher
    success_url = reverse_lazy('teachers:list_teachers')
    form_class = TeacherUpdateForm
    template_name = 'teachers/edit_teacher.html'
    pk_url_kwarg = 'id'


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    pk_url_kwarg = 'id'
    template_name = 'teachers/delete_teacher.html'
    success_url = reverse_lazy('teachers:list_teachers')