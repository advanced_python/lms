import random

from django.db import models
from django.urls import reverse
from faker import Faker
from core_lms.models import Person


class Teacher(Person):
    gender = models.CharField(max_length=64, null=False, default='MALE')
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    class Role(models.IntegerChoices):
        TEACHER = 1, 'Teacher'
        MENTOR = 2, 'Mentor'

    roles = models.PositiveIntegerField(default=Role.TEACHER,
                                            choices=Role.choices)


    @classmethod
    def generate_teachers(cls, count):
        faker = Faker()

        for _ in range(count):
            t = Teacher()
            t.gender = random.choice(["MALE", "FEMALE"])
            t.first_name = faker.first_name_male() if t.gender == "MALE" else faker.first_name_female()
            t.last_name = faker.last_name()
            t.email = faker.ascii_free_email()
            t.birth_date = faker.date_of_birth()
            t.phone_number = faker.phone_number()
            t.save()

    def __str__(self):
        return f"Teacher id - ({self.id}): {self.first_name} {self.last_name} : " \
               f"{self.gender} : {self.email} : {self.phone_number}"

    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'

    def get_update_link(self):
        return reverse('teachers:update_teacher',kwargs={'id':self.id})
