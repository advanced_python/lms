class frange:
    def __init__(self, start, end, step):
        self.start = start
        self.end = end
        self.step = step
        self.__current = start - step

    def __iter__(self):
        return self

    def __next__(self):
        self.__current += self.step

        if self.__current > self.end:
            raise StopIteration

        return self.__current



for i in frange(1, 99, 3):
    print(i)

 #  ---------------------------------------------------

# class Fibonacci:
#     def __init__(self):
#         self.a = 0
#         self.b = 1
#
#     def __iter__(self):
#         return self
#
#     def __next__(self):
#
#             self.a, self.b = self.b, self.a + self.b
#             return self.a
#
#
# for index, item in enumerate(Fibonacci(), start=1):
#   if index > 10:
#     break
#   print(index, item)