from django.contrib import messages
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.context_processors import request

from teachers.models import Teacher


@receiver(post_save, sender=Teacher)
def added_teacher(sender, instance, created, **kwargs):
    print(f'---------- Teacher:: {instance} created successfully --------------------')

    # messages.INFO(
    #     request,
    #     f'Teacher {instance} created successfully'
    # )

    # messages.add_message(request, messages.INFO, f'Teacher {instance} created successfully')