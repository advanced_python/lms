# Generated by Django 3.2.11 on 2022-01-19 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='age',
            field=models.CharField(max_length=64, null=True),
        ),
    ]
