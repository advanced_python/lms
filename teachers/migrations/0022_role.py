# Generated by Django 3.2.11 on 2022-02-18 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0021_auto_20220217_1901'),
    ]

    operations = [
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('roles', models.CharField(choices=[('Teacher', 'Teacher'), ('Mentor', 'Mentor')], default='Teacher', max_length=7)),
            ],
        ),
    ]
