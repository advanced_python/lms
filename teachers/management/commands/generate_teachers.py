from teachers.models import Teacher
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        number = options['count']
        try:
            Teacher.generate_teachers(int(number))
        except TypeError:
            raise (f'This argument - {number}, is not number')
