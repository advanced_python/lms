from django.urls import path

from teachers.views import TeacherEditView, TeacherDeleteView, TeacherCreateView, TeacherListView

app_name = 'teachers'

urlpatterns = [
    path('', TeacherListView.as_view(), name='list_teachers'),
    path('create', TeacherCreateView.as_view(), name='create_teacher'),
    path('update/<int:id>', TeacherEditView.as_view(), name='update_teacher'),
    path('delete/<int:id>', TeacherDeleteView.as_view(), name='delete_teacher'),
]
